<?php

namespace Drupal\collection\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Prevents an uncollectible item from being added to a collection.
 *
 * @Constraint(
 *   id = "PreventUncollectible",
 *   label = @Translation("Prevent uncollectible collection", context = "Validation"),
 *   type = "string"
 * )
 */
class PreventUncollectible extends Constraint {

  public $uncollectible = '%entity cannot be collected.';

}
