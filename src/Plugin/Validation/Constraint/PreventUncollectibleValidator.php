<?php

namespace Drupal\collection\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the PreventUncollectible constraint.
 */
class PreventUncollectibleValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($collection_item, Constraint $constraint) {
    if ($collection_item->item->entity->hasField('collectible') === FALSE) {
      return;
    }

    if ($collection_item->item->entity->collectible->value == FALSE) {
      $this->context->addViolation($constraint->uncollectible, [
        '%entity' => $collection_item->item->entity->label(),
      ]);
    }
  }

}
