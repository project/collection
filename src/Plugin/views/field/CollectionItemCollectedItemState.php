<?php

namespace Drupal\collection\Plugin\views\field;

use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide the workflow state of the collected item entity.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("collection_item_collected_item_state")
 */
class CollectionItemCollectedItemState extends FieldPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a CollectionItemCollectedItemState object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The id of the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    if (!\Drupal::hasService('content_moderation.moderation_information')) {
      return '';
    }

    /** @var \Drupal\content_moderation\ModerationInformationInterface $moderation_info */
    $moderation_info = \Drupal::service('content_moderation.moderation_information');

    $collection_item = $this->getEntity($row);

    // Collected items are dynamic entity references, and they always return the
    // default revision. However, we want the state of the latest revision of
    // the entity, which may be a draft.
    if ($moderation_info->isModeratedEntity($collection_item->item->entity)) {
      $storage = $this->entityTypeManager->getStorage($collection_item->item->entity->getEntityTypeId());
      $latest_revision_id = $storage->getLatestTranslationAffectedRevisionId($collection_item->item->entity->id(), $collection_item->item->entity->language()->getId());
      $latest_revision = $storage->loadRevision($latest_revision_id);
      $workflow_type = $moderation_info->getWorkflowForEntity($collection_item->item->entity)->getTypePlugin();

      // Get the human readable label for the workflow state.
      if ($workflow_type->hasState($latest_revision->moderation_state->value)) {
        return $workflow_type->getState($latest_revision->moderation_state->value)->label();
      }
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Override the parent query function, since this is a computed field.
  }

}
