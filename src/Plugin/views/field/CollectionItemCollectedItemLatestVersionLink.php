<?php

namespace Drupal\collection\Plugin\views\field;

use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Field handler to present a link to a collected entity's latest version.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("collection_item_collected_item_latest_version_link")
 */
class CollectionItemCollectedItemLatestVersionLink extends LinkBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    return $this->getEntity($row) ? parent::render($row) : [];
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    if ($this->options['output_url_as_text']) {
      return $this->getUrlInfo($row)->toString();
    }
    return parent::renderLink($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    $collection_item = $this->getEntity($row);
    $entity = $collection_item->item->entity;

    // If not a content entity, then let Drupal handle it with defaults.
    if (!$entity instanceof ContentEntityInterface) {
      return $entity->toUrl();
    }

    /** @var \Drupal\content_moderation\ModerationInformationInterface $moderation_info */
    $moderation_info = \Drupal::service('content_moderation.moderation_information');

    $template = $moderation_info->hasPendingRevision($entity) ? 'latest-version' : 'canonical';

    if (!$entity->getEntityType()->hasLinkTemplate($template)) {
      return $entity->toUrl();
    }

    return $entity->toUrl($template);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['output_url_as_text'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['output_url_as_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Output the URL as text'),
      '#default_value' => $this->options['output_url_as_text'],
    ];
    parent::buildOptionsForm($form, $form_state);
    // Only show the 'text' field if we don't want to output the raw URL.
    $form['text']['#states']['visible'][':input[name="options[output_url_as_text]"]'] = ['checked' => FALSE];
  }

  /**
   * Override the default label.
   *
   * @return string
   *   The default link label.
   */
  protected function getDefaultLabel() {
    return $this->t('Latest version');
  }
}
