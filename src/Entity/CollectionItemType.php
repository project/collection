<?php

namespace Drupal\collection\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Collection item type entity.
 *
 * @ConfigEntityType(
 *   id = "collection_item_type",
 *   label = @Translation("Collection item type"),
 *   label_collection = @Translation("Collection item types"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\collection\CollectionItemTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\collection\Form\CollectionItemTypeForm",
 *       "edit" = "Drupal\collection\Form\CollectionItemTypeForm",
 *       "delete" = "Drupal\collection\Form\CollectionItemTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "collection_item_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "collection_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/collection_item/add",
 *     "edit-form" = "/admin/structure/collection_item/{collection_item_type}",
 *     "delete-form" = "/admin/structure/collection_item/{collection_item_type}/delete",
 *     "collection" = "/admin/structure/collection_item"
 *   },
 *   config_prefix = "collection_item_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "allowed_bundles",
 *     "attributes_access",
 *   }
 * )
 */
class CollectionItemType extends ConfigEntityBundleBase implements CollectionItemTypeInterface {

  /**
   * The Collection item type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Collection item type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Collection item allowed content entity bundles.
   *
   * @var array
   */
  protected $allowed_bundles = [];


  /**
   * The Collection item attributes access setting.
   *
   * @var string
   */
  protected $attributes_access;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Enable and configure the `mini` form display for newly created collection
    // item types.
    if (!$update) {
      /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
      $display_repository = \Drupal::service('entity_display.repository');

      // The `mini` form mode is created during install, but can be removed and
      // therefore might not exist.
      $form_modes = \Drupal::service('entity_display.repository')->getFormModes('collection_item');

      if (isset($form_modes['mini'])) {
        // Disable the collected item in the mini form mode, which is used for
        // collection item inline entity forms.
        $display_repository->getFormDisplay('collection_item', $this->id(), 'mini')
          ->removeComponent('item')
          ->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedBundles() {
    return $this->allowed_bundles;
  }
}
