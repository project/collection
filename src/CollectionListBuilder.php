<?php

namespace Drupal\collection;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Collection entities.
 *
 * @ingroup collection
 */
class CollectionListBuilder extends EntityListBuilder {

  /**
   * The Moderation Information service, if available.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|NULL
   */
  protected $moderationInfo;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('content_moderation.moderation_information', ContainerInterface::NULL_ON_INVALID_REFERENCE)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ModerationInformationInterface $moderation_info = NULL) {
    parent::__construct($entity_type, $storage);
    $this->moderationInfo = $moderation_info;
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_ids = $this->getEntityIds();
    $loaded_collections = $this->storage->loadMultipleRevisions(array_keys($entity_ids));
    $displayed_collections = [];

    foreach ($loaded_collections as $collection) {
      if ($collection->access('view')) {
        $displayed_collections[] = $collection;
      }
    }

    return $displayed_collections;
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->entityType->getKey('label'));

    if ($this->moderationInfo) {
      $query->latestRevision();
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['type'] = $this->t('Type');
    $header['collectible'] = $this->t('Collectible');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\collection\Entity\Collection $entity */
    $row['name'] = $entity->toLink();
    $row['type'] = $entity->bundle();
    $row['collectible'] = $entity->collectible->value ? '✓' : '';
    if ($this->moderationInfo && $workflow = $this->moderationInfo->getWorkflowForEntity($entity)) {
      $row['status'] = $workflow->getTypePlugin()->getState($entity->moderation_state->value)->label();
    }
    else {
      $row['status'] = $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished');
    }
    return $row + parent::buildRow($entity);
  }

}
