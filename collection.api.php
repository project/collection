<?php

/**
 * @file
 * Hooks related to Collection module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the allowed collection_item types for a given collection type and item.
 *
 * If an entity_type/bundle combo, e.g. node/article is allowed in multiple
 * collection_item types for a given collection type, this hook can be used to
 * alter that list of allowed collection_item types.
 *
 * For example, a Blog collection type might allow two collection item types:
 * 'Blog item' and 'Awesome item'. If both 'Blog item' and 'Awesome item' types
 * allow Article nodes, which one should be prefered?
 *
 * CollectionType::getAllowedCollectionItemTypes(), used by the 'Add new
 * content' link on the collected items listing, returns a list of allowed
 * collection item types for a given entity_type and bundle. If multiple types
 * are available, they are returned in alphabetical order.
 *
 * In this example, 'Awesome item' would be used since it's returned first
 * alphabetically. But if the 'Blog item' collection_item type is preferred, it
 * can be moved to the top of the $allowed_types array. Or it could be removed
 * altogether.
 *
 * @param array $allowed_types
 *   An array of collection_item type IDs allowed in the $collection_type_id,
 *   and in which the $entity_type_id and $bundle are allowed as per config.
 * @param string $collection_type_id
 *   A collection type ID.
 * @param string $entity_type_id
 *   The entity type ID (e.g. node) for the collected item.
 * @param string $bundle
 *   The bundle name (e.g. article) for the collected item
 */
function hook_collection_item_types_allowed_alter(&$allowed_types, $collection_type_id, $entity_type_id, $bundle) {
  if ($collection_type_id === 'blog' && $entity_type_id === 'node' && $bundle === 'article') {
    usort($allowed_types, function($a, $b) {
      return $a === 'blog_item' ? -1 : 1;
    });
  }
}

/**
 * @} End of "addtogroup hooks".
 */
